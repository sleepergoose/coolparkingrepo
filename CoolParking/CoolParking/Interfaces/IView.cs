﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;

namespace CoolParking.Interfaces
{
    // Contract to ensure interaction between the presenter and the console interface
    interface IView
    {
        void ShowMain(List<string> Menu, bool withInviting = true);
        void ShowOptions(List<string> options, bool withInviting = true);
        void ShowCurrentParkingBalance(decimal balance);
        void ShowCurrentIncome(decimal income);
        void ShowNumberParkingSpaces(int freePlaces, int occupiedPlaces);
        void ShowCurrentTransactions(TransactionInfo[] transactionInfo);
        void ShowTransactionsHistory(string history);
        void ShowVehiclesList(ReadOnlyCollection<Vehicle> vehicles);
        Vehicle ShowPutVehicle();
        string[] ShowPickUpVehicle(ReadOnlyCollection<Vehicle> vehicles);
        IEnumerable<(string id, decimal sum)> ShowTopUpVehicleBalance(ReadOnlyCollection<Vehicle> vehicles);
        void ShowSettings();
        void WriteTextToCenter(string text);
        void WriteErrorToCenter(string text);
        string GetTextResponse();
        string GetKeyResponce();
    }
}