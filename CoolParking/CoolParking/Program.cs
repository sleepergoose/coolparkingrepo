﻿using CoolParking.BL.Services;
using System.IO;
using System.Reflection;

namespace CoolParking
{
    class Program
    {
        private static readonly string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        static void Main(string[] args)
        {
            // Realizes dependency inversion
            View view = new View();
            TimerService withdrawTimer = new TimerService();
            TimerService logTimer = new TimerService();
            LogService logService = new LogService(logFilePath);

            Presenter presenter = new Presenter(view: view, withdrawTimer: withdrawTimer, logTimer: logTimer, logService: logService);
            presenter.Run();
        }
    }
}
