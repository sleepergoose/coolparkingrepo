﻿using System;
using System.Collections.Generic;
using CoolParking.Interfaces;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System.Linq;
using System.IO;

namespace CoolParking
{
    public class Presenter
    {
        delegate void Manager(string response = "");
        Manager _globalManager;
        Manager _optionsManager;

        private readonly IView _view = null;
        private IParkingService _parkingService = null;
        private readonly ITimerService _withdrawTimer = null;
        private readonly ITimerService _logTimer = null;
        private readonly ILogService _logService = null;

        private List<string> Menu = new List<string>()
        {
            "Menu (F3)",
            "Run parking  (F5)",
            "Stop parking (F7)",
            "Quit (F9)"
        };
        private List<string> Options = new List<string>()
        {
            "Current parking balance",
            "Income in the current period",
            "Number of free/occupied parking spaces",
            "Transactions for the current period",
            "Transactions history",
            "List of vehicles in the Parking lot",
            "Put the vehicle on the Parking lot",
            "Pick up a vehicle from the Parking lot",
            "Top up the balance of a vehicle",
            "Settings"
        };

        private readonly string _logFilePath;

        public Presenter(View view, TimerService withdrawTimer, TimerService logTimer, LogService logService)
        {
            _view = view;
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _logFilePath = _logService.LogPath;

            _globalManager = MainManager;
            _optionsManager = OptionsManager;
        }

        // Starts manager
        public void Run()
        {
            _globalManager();
        }

        /// <summary>
        /// The main application's dispatcher.
        /// </summary>
        /// <param name="text">This is an optional parameter</param>
        void MainManager(string text = "")
        {
            _view.ShowMain(Menu);
            if (string.IsNullOrEmpty(text) == false)
            {
                _view.WriteTextToCenter(text);
            }
            string res = _view.GetKeyResponce();

            switch (res)
            {
                case "F3":
                    ShowOptions_F3();
                    break;
                case "F5":
                    StartParkingService_F5();
                    break;
                case "F7":
                    StopParkingService_F7();
                    break;
                case "F9":
                    // Good Bye!
                    AppExit_F9();
                    break;
                default:
                    break;
            }
        }


#region Global Manager Supporting Functions

        /// <summary>
        /// Loads Options menu
        /// </summary>
        private void ShowOptions_F3()
        {
            _view.ShowOptions(Options);
            _optionsManager(_view.GetTextResponse());
        }

        /// <summary>
        /// Starts Parking Service
        /// </summary>
        private void StartParkingService_F5()
        {
            if(_parkingService == null)
            {
                _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
                _globalManager("Parking Service is started!");
            }
            else
            {
                _globalManager("Parking Service is already running!");
            }
        }

        /// <summary>
        /// Stops Parking Service
        /// </summary>
        private void StopParkingService_F7()
        {
            if(_parkingService != null)
            {
                _withdrawTimer.Stop();
                _logTimer.Stop();

                if (File.Exists(_logService.LogPath) == true)
                {
                    File.Delete(_logService.LogPath);
                }

                _parkingService = null;
                _globalManager("Parking Service is stoped!");
            }
            else
            {
                _globalManager("Parking Service is already stoped!");
            }
        }

        /// <summary>
        /// Exits from application
        /// </summary>
        private void AppExit_F9()
        {
            if (_parkingService != null)
            {
                _parkingService.Dispose();
            }
            else
            {
                _view.WriteTextToCenter("Good Luck!");
                _view.WriteTextToCenter("...Press 'Enter' key to exit...");
                Console.ReadLine();
            }
        }

        #endregion

        /// <summary>
        /// Options manager. It's in charge of the Options menu 
        /// </summary>
        /// <param name="command"></param>
        void OptionsManager(string command)
        {
            switch (command)
            {
                case "1" when _parkingService != null: /* Current parking balance */
                    _view.ShowCurrentParkingBalance(_parkingService.GetBalance());
                    break;
                case "2" when _parkingService != null: /* Income in the current period */
                    _view.ShowCurrentIncome(_parkingService.GetLastParkingTransactions().Sum(p => p.Sum));
                    break;
                case "3" when _parkingService != null: /* Number of free/occupied parking spaces */
                    _view.ShowNumberParkingSpaces(_parkingService.GetFreePlaces(), _parkingService.GetCapacity() - _parkingService.GetFreePlaces());
                    break;
                case "4" when _parkingService != null: /* ShowCurrentTransactions */
                    ShowCurrentTransaction();
                    break;
                case "5" when _parkingService != null: /* Transactions history */
                    ShowTransactionHistory();
                    break;
                case "6" when _parkingService != null: /* List of vehicles in the Parking lot */
                    ShowVehiclesList();
                    break;
                case "7" when _parkingService != null: /* Put the vehicle on the Parking lot */
                    PutVehicle();
                    break;
                case "8" when _parkingService != null: /* Pick up a vehicle from the Parking lot */
                    PickUpVehicle();
                    break;
                case "9" when _parkingService != null: /* Top up the balance of a vehicle */
                    TopUpVehicle();
                    break;
                case "10":
                    _view.ShowSettings();
                    break;
                case "menu": /* Return to main menu */
                    _globalManager();
                    return;
                default: /* If entered wrong command */
                    _view.WriteErrorToCenter("Entered wrong command or this command is permitted when Parking is running !");
                    break;
            }
            _optionsManager(_view.GetTextResponse());
        }


#region Option manager supporting functions

        /// <summary>
        /// Shows a list with all vehicles on the Parking
        /// </summary>
        private void ShowVehiclesList()
        {
            _view.ShowVehiclesList(_parkingService.GetVehicles());
        }

        /// <summary>
        /// Shows a list with current transactions
        /// </summary>
        private void ShowCurrentTransaction()
        {
            try
            {
                _view.ShowCurrentTransactions(_parkingService.GetLastParkingTransactions());
            }
            catch (Exception)
            {
                _view.WriteErrorToCenter("Something is wrong. Current transaction cannot be loaded");
            }
        }

        /// <summary>
        /// Shows transaction history
        /// </summary>
        private void ShowTransactionHistory()
        {
            try
            {
                _view.ShowTransactionsHistory(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                _view.ShowTransactionsHistory("Transactions history is empty");
            }
            catch (Exception)
            {
                _view.WriteErrorToCenter("Something is wrong. Transaction history cannot be loaded");
            }
        }

        /// <summary>
        /// Shows screen to add a vehicle to the Parking
        /// </summary>
        private void PutVehicle() 
        {
            try
            {
                Vehicle vehicle = _view.ShowPutVehicle();
                if (vehicle != null)
                {
                    _parkingService.AddVehicle(vehicle);
                    _view.WriteTextToCenter($"The {vehicle.VehicleType} {vehicle.Id} was added");
                }
                else
                {
                    _view.WriteErrorToCenter($"Last vehicle is not added. Something was wrong. Please, try again.");
                }
            }
            catch (ArgumentException)
            {
                _view.WriteErrorToCenter("Entered vehicle parameters were incorrect! Operation aborted");
            }
            catch (Exception)
            {
                _view.WriteErrorToCenter("Something was wrong! Operation aborted");
            }
        }

        /// <summary>
        /// Shows screen to pick up a vehicle from the Parking
        /// </summary>
        private void PickUpVehicle()
        {
            try
            {
                string[] vehicleIds = _view.ShowPickUpVehicle(_parkingService.GetVehicles());
                if (vehicleIds != null && vehicleIds.Length != 0)
                {
                    foreach (string id in vehicleIds)
                    {
                        if (string.IsNullOrEmpty(id) == false)
                        {
                            _parkingService.RemoveVehicle(id);
                            _view.WriteTextToCenter($"The vehicle with Id {id} was removed");
                        }
                    }
                }
                else
                {
                    _view.WriteTextToCenter($"None vehicle was selected. Please, try again.");
                }
            }
            catch (InvalidOperationException ex)
            {
                _view.WriteErrorToCenter(ex.Message);
            }
            catch (ArgumentException ex)
            {
                _view.WriteErrorToCenter(ex.Message);
            }
            catch (Exception)
            {
                _view.WriteErrorToCenter($"None vehicle was removed. Please, try again.");
            }
        }

        /// <summary>
        /// Shows screen to top up money to a vehicle balance
        /// </summary>
        private void TopUpVehicle()
        {
            IEnumerable<(string id, decimal sum)> list = _view.ShowTopUpVehicleBalance(_parkingService.GetVehicles());

            try
            {
                if(list != null && list.Count() != 0)
                {
                    foreach (var item in list)
                    {
                        _parkingService.TopUpVehicle(item.id, item.sum);
                    }
                    ShowVehiclesList();
                }
                else
                {
                    _view.WriteErrorToCenter($"None vehicle was toped up. Please, try again.");
                }
            }
            catch (ArgumentException ex)
            {
                _view.WriteErrorToCenter(ex.Message);
            }
            
        }
        #endregion
    }
}
