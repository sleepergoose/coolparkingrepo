﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        /// <summary>
        /// Class constructor
        /// </summary>
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex regex = new Regex(Settings.VehicleIdPatern);
            if (regex.IsMatch(id) == false)
            {
                throw new ArgumentException("Vehicle constructor: id has a wrong format! It must have a format like AB-1234-CD");
            }
            else if (balance < 0)
            {
                throw new ArgumentException("Vehicle constructor: The balance argument must be greater than zero!");
            }
            else
            {
                this.Id = id;
                this.VehicleType = vehicleType;
                this.Balance = balance;
            }
        }

        /// <summary>
        /// Static method GenerateRandomRegistrationPlateNumber returns a randomly generated unique identifier.
        /// </summary>
        /// <returns>Unique vehicle identifier</returns>
        public static string GenerateRandomRegistrationPlateNumber()
        {
            try
            {
                Regex regex = new Regex(Settings.VehicleIdPatern);
                var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                StringBuilder stringBuilder = new StringBuilder();
                Random random = new Random();
                do
                {
                    // Generates the first two characters
                    stringBuilder.Append(string.Concat(Enumerable.Range(0, 2).Select(p => letters[random.Next(0, letters.Length)])));
                    stringBuilder.Append('-');
                    // Generates four digit symbols 
                    stringBuilder.Append(random.Next(1, 9999).ToString().PadLeft(4, '0'));
                    stringBuilder.Append('-');
                    // Generates the last two characters
                    stringBuilder.Append(string.Concat(Enumerable.Range(0, 2).Select(p => letters[random.Next(0, letters.Length)])));

                } while (regex.IsMatch(stringBuilder.ToString()) == false);
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Can't generate a new unique vehicle identifier");
                throw new Exception(ex.Message);
            }
        }
    }
}