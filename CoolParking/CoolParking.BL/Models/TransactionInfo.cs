﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// The transaction records each fact of payment
    /// </summary>
    public struct TransactionInfo
    {
        public DateTime TransactionTime { get; }
        public string VehicleId { get; }
        public decimal Sum { get; }

        public TransactionInfo(DateTime transactionTime, string vehicleId, decimal sum)
        {
            this.TransactionTime = transactionTime;
            this.VehicleId = vehicleId;
            this.Sum = sum;
        }

        public override string ToString()
        {
            return  $"Time: {TransactionTime.ToLongTimeString()} | " +
                    $"Id: {VehicleId} | " +
                    $"Sum: {Sum:C}";
        }
    }
}