﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;
        private double interval;

        public double Interval
        {
            get
            {
                return interval;
            }
            set
            {
                if (value > 0 && value < int.MaxValue)
                    interval = value;
                else
                    throw new Exception("Wrong value of the property Interval");
            }
        }

        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {
            timer = new Timer();
        }

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Interval = interval;
            timer.Elapsed += this.Elapsed;
            timer.AutoReset = true;
            // Start the timer
            timer.Enabled = true;
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}